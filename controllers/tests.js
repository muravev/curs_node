'use strict';
const express = require('express');
const app = express();

let lastId = 0;
const tests = {};


app.get('/(:test_id)?', (req, res, next) => {
    let id = req.params.test_id;
    if(id) {
        return tests[id] ? res.send(tests[id]) : res.status(404).send('Question not found');
    }

    return res.send(Object.keys(tests).map(k => tests[k]));
});

app.post('/', (req, res, next) => {
    if(!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }
    tests[++lastId] = req.body;
    tests[lastId].id = lastId;

    console.log(tests);
    return res.send(tests[lastId]);
});

app.put('/:test_id', (req, res, next) => {
    if(!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let id = req.params.test_id;
    if(!tests[id]) {
        return res.status(404).send('Question not found');
    }

    Object.keys(req.body).map(k => tests[id][k] = req.body[k]);

    return res.send(tests[id]);
});

app.delete('/:test_id', (req, res, next) => {
    let id = req.params.test_id;
    if(!tests[id]) {
        return res.status(404).send('Question not found');
    }

    delete tests[id];

    return res.send();
});

module.exports = app;

