'use strict';
const express = require('express');
const app = express();

//пример вопроса
let lastId = 1;
const questions = {
    1: {
        id: 1,
        legend: [
            `Книга №1, упавша со стола на пол, обладает в момент падения кинетической энергией [%a] Дж. 
        Высота стола - [%b] м. Чему равна масса книги? Сопротивление воздуха пренебречь`,
            `Книга №2, упавша со стола на пол, обладает в момент падения кинетической энергией [%a] Дж. 
        Высота стола - [%b] м. Чему равна масса книги? Сопротивление воздуха пренебречь`],
        formula: '[%a]+[%b]*145',
        id_section: 1,
        limitation: {
            '%a': {
                from: 2,
                to: 20
            },
            '%b': {
                from: 2,
                to: 20
            }
        }
    }
};


app.get('/(:question_id)?', (req, res, next) => {
    let id = req.params.question_id;
    if(id) {
        return questions[id] ? res.send(questions[id]) : res.status(404).send('Question not found');
    }

    return res.send(Object.keys(questions).map(k => questions[k]));
});

app.post('/', (req, res, next) => {
    if(!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }
    questions[++lastId] = req.body;
    questions[lastId].id = lastId;

    console.log(questions);
    return res.send(questions[lastId]);
});

app.put('/:question_id', (req, res, next) => {
    if(!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let id = req.params.question_id;
    if(!questions[id]) {
        return res.status(404).send('Question not found');
    }

    Object.keys(req.body).map(k => questions[id][k] = req.body[k]);

    return res.send(questions[id]);
});

app.delete('/:question_id', (req, res, next) => {
    let id = req.params.question_id;
    if(!questions[id]) {
        return res.status(404).send('Question not found');
    }

    delete questions[id];

    return res.send();
});

module.exports = app;

