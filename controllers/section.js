'use strict';
const express = require('express');
const app = express();

let lastId = 0;
const sections = {};


app.get('/(:section_id)?', (req, res, next) => {
    let id = req.params.section_id;
    if(id) {
        return sections[id] ? res.send(sections[id]) : res.status(404).send('Question not found');
    }

    return res.send(Object.keys(sections).map(k => sections[k]));
});

app.post('/', (req, res, next) => {
    if(!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }
    sections[++lastId] = req.body;
    sections[lastId].id = lastId;

    console.log(sections);
    return res.send(sections[lastId]);
});

app.put('/:section_id', (req, res, next) => {
    if(!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let id = req.params.section_id;
    if(!sections[id]) {
        return res.status(404).send('Question not found');
    }

    Object.keys(req.body).map(k => sections[id][k] = req.body[k]);

    return res.send(sections[id]);
});

app.delete('/:section_id', (req, res, next) => {
    let id = req.params.section_id;
    if(!sections[id]) {
        return res.status(404).send('Question not found');
    }

    delete sections[id];

    return res.send();
});

module.exports = app;

