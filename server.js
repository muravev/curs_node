'use strict';

var express = require('express');
var bodyParser  = require('body-parser');
var cors = require('cors');
var question = require('./controllers/question.js');
var section = require('./controllers/section.js');
var test = require('./controllers/tests.js');

var app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

app.use('/question', question);
app.use('/section', section);
app.use('/test', test);